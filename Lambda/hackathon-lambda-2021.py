import json
import boto3
import csv
import mysql.connector

s3 = boto3.resource('s3')
def lambda_handler(event, context):
    try:
       s3_bucket = event['Records'][0]['s3']['bucket']['name']
       s3_key = event['Records'][0]['s3']['object']['key']
       s3_file_name = s3_key.split("/")[-1]
       download_location =  f'/tmp/{s3_file_name}'
       s3.meta.client.download_file(s3_bucket, s3_key, download_location)
       col_name = ["date", "campaign", "impression", "clicks", "transactions", "company"]
       with open(download_location) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        array_list = []
        for row in csv_reader:
            print(row)
            if line_count == 0:
                company_name = row[0].split("_")[0]
                line_count = line_count + 1
            else:
                row_to_mysql = [row[0], row[1], row[2], row[3], row[4] ,company_name]
                db_data_json = {
                        "advt_date": row[0],
                        "advt_campaign_name": row[1],
                        "advt_impression": row[2],
                        "advt_clicks": row[3],
                        "advt_transation": row[4],
                        "advt_company_name": company_name
                    }
                array_list.append(db_data_json)
                
                connection = mysql.connector.connect(host='hackathon-mysql-2021.cuiywmqvw3ap.ap-south-1.rds.amazonaws.com',
                                         database='Hackathon_Database_2021',
                                         port='3306',
                                         user='admin',
                                         passwd='YOG2MYSQL')
                mysql_insert = "insert into Hackathon_Table_2021(advt_date, advt_campaign_name, advt_impression , advt_clicks,  advt_transation , advt_company_name ) values(%s,%s,%s,%s,%s,%s)"
                cursor = connection.cursor()
                cursor.execute(mysql_insert, row_to_mysql)
                connection.commit()
                print("Row count-  ", cursor.rowcount)
                
                
        for _data in array_list:
            print(f"Output json {json.dumps(_data)}")
    except Exception as ex:
        print(ex)
    
   
