import json
import boto3
import mysql.connector
from datetime import date, datetime

def lambda_handler(event, context):
    # TODO implement
    
    try:
        
        connection = mysql.connector.connect(host='hackathon-mysql-2021.cuiywmqvw3ap.ap-south-1.rds.amazonaws.com',
                                         database='Hackathon_Database_2021',
                                         port='3306',
                                         user='admin',
                                         passwd='YOG2MYSQL')
                                         
        my_sql_get_all_data = "select * from Hackathon_Table_2021"
        val = ("adwords",)
        cursor = connection.cursor()
        cursor.execute(my_sql_get_all_data)
        adword_advt_data = list(sum(cursor.fetchall(), ()))
        
        print("Row count-  ", cursor.rowcount)
        print("\n adword_advt_data =\t {}----\n".format(adword_advt_data)) 
        
        def json_serial(obj):
            """JSON serializer for objects not serializable by default json code"""
        
            if isinstance(obj, (datetime, date)):
                return obj.isoformat()
            raise TypeError ("Type %s not serializable" % type(obj))

        
        for index, item in enumerate(adword_advt_data):
        	if isinstance(item, (datetime, date)):
        		adword_advt_data[index] = json_serial(item)

        print("\n 2- adword_advt_data =\t {}----\n".format(adword_advt_data))
        
        print("\n json.dumps(adword_advt_data) =\t {}----\n".format(json.dumps(adword_advt_data)))                   
        
        return {
            'statusCode': 200,
            'body': json.dumps(adword_advt_data)
        }
        
    except Exception as ex:
        print("\n Exception Error =\t {}----\n".format(ex))
